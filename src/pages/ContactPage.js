import React, {Component} from "react"
import Breadcrumb from "../components/Breadcrumb";
import {API, SITE_NAME} from "../config";
import {Helmet} from "react-helmet";

export  default class ContactPage extends Component{


    onSendForm(){

        const field = (id) => document.getElementById(`forms-${id}`).value;

        API.send.form({
            type: "Contact from",
            "full name": `${field("name")} ${field("last-name")}`,
            "message": field("message"),
            "mail": field("email")
        })
    }
    render() {
        return <>
        <Helmet>
            <title>{`Contact me | ${SITE_NAME}`}</title>
            <meta name="description" content="
                TODO
                "/>
            <meta name="keywords" content="Video, photo, photographer, filmmaking, Prague, "/>
            <meta name="author" content="Alikhan Mexman"/>
            <styles>{`
            .jumbotron.text-center * {
                color: black;
            }
`}</styles>
        </Helmet>
            <section className="text-center">
                <div className="jumbotron text-center margin-large">
                    <h1>
                        <small>GET IN TOUCH</small>
                        Let's Create!
                    </h1>
                </div>
                <Breadcrumb title="Contact"/>
            </section>
            {/***************************************************************************************/}
            {/*                                       CONTACT FORM                                  */}
            {/***************************************************************************************/}
            <section className="well well-sm">
            <div className="container">
                <div className="row row-30">
                    <div className="col-lg-8">
                        <div className="button-shadow bg-default py-5 px-3 round-large">
                            <h5 className="text-center">GET IN TOUCH</h5>
                            <form className="rd-mailform text-left" data-form-output="form-output-global"
                                  data-form-type="forms" onSubmit={this.onSendForm}>
                                <div className="row row-20 align-items-end">
                                    <div className="col-md-6">
                                        <div className="form-wrap form-wrap-validation validation-with-outside-label">
                                            <label className="form-label-outside" htmlFor="forms-name">First
                                                name</label>
                                            <input className="form-input" id="forms-name" type="text" name="name"
                                                   placeholder="Your First Name" data-constraints="@Required"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-wrap form-wrap-validation validation-with-outside-label">
                                            <label className="form-label-outside" htmlFor="forms-last-name">Last
                                                name</label>
                                            <input className="form-input" id="forms-last-name" type="text"
                                                   name="last-name"
                                                   placeholder="Your Last Name" data-constraints="@Required"/>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="form-wrap form-wrap-validation validation-with-outside-label">
                                            <label className="form-label-outside" htmlFor="forms-message">Your
                                                message</label>
                                            <textarea className="form-input" id="forms-message" name="message"
                                                      placeholder="Write your message here"
                                                      data-constraints="@Required"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-wrap form-wrap-validation validation-with-outside-label">
                                            <label className="form-label-outside" htmlFor="forms-email">E-mail</label>
                                            <input className="form-input" id="forms-email" type="email" name="email"
                                                   placeholder="Enter your e-mail" data-constraints="@Email @Required"/>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <button
                                            className="button button-primary button-xs round-xl button-block form-el-offset-1"
                                            type="submit">Send Message
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="col-lg-4 text-center text-lg-left">
                        <address className="contact-block bg-default button-shadow py-5 px-4 round-large">
                            <dl className="list-md">
                                <dt className="heading-6 text-uppercase">PHONE</dt>
                                <dd><a href="tel:+‭420 775 129 495‬">‭+420 775 129 495‬</a></dd>
                                <dt className="heading-6 text-uppercase">E-MAIL</dt>
                                <dd><a href="mailto:contact@alikhanmexman.com">contact@alikhanmexman.com</a></dd>
                            </dl>
                            <ul className="list-inline list-inline-3">
                                <li><a className="fa-twitter" href="#"/></li>
                                <li><a className="fa-instagram" href="https://twitter.com/ALIKHANTHEONE"/></li>
                                <li><a className="fa-facebook" href="#"/></li>
                            </ul>
                        </address>
                    </div>
                </div>
            </div>
        </section>
        </>
    }
}