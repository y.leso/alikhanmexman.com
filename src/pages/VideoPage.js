import React, {Component} from "react";
import $ from "jquery";
import {Helmet} from "react-helmet";
import Loading from "../components/Loading";
import {API, DBG_MSG, mountJS, SITE_NAME} from "../config";
import VideoCategoryPage from "./VideoCategoryPage";
import SlideshowSection from "../components/SlideshowSection";

export default class VideoPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: undefined
        };

        this.loadVideo().then(DBG_MSG);
    }

    componentDidUpdate() {
        mountJS("https://player.vimeo.com/api/player.js")
    }

    async loadVideo() {
        const {url, category} = this.props.match.params;
        const item = await API.get.video(VideoCategoryPage.categoryURLToName(category), url);

        if (item.length > 0) {
            item[0].photo.path = await API.get.vimeoImgUrl(item[0].video);
            this.setState({item: item[0]})
        }else this.setState({item: null})


    }

    static getVideoImage(videoUrl) {

    }

    render() {
        const {item} = this.state;
        if (item === undefined) return <Loading/>;

        const {photo, content, title, video, _created, tags} = item;
        return <>
            <Helmet>
                <title>{`${VideoCategoryPage
                    .categoryURLToName(this.props.match.params.category) || "All"}  | Videos | ${SITE_NAME}`}</title>
                <meta name="description" content={$(content).text().substr(0, 180)}/>
                <meta name="keywords" content={tags}/>
                <meta name="author" content="Alikhan Mexman"/>
                <style>{`
                .swiper-slide.swiper-slide-active:after {
                    content: "";
                    display: block;
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    background: #ffffff8c;
                }
                div#video {
                    width: 100%;
                    max-width: 55rem;
                    margin: 0 auto;
                    margin-top: -9rem;
                    position: relative;
                    z-index: 9;
                    height: ${window.innerWidth / 2}px;
                    background: black;
                    box-shadow: -3px 7px 20px 9px;
                }

                div#video iframe {
                    width: 100%;
                    height: 100%;
                }
                #content {
                    margin: .5rem 0;
                }
                #content > :not(div){
                    max-width: 80ch;
                    margin-left: auto;
                    margin-right: auto
                }
                #content h2, #content h3 {
                    max-width: calc(80ch/(28/12.85));
                }

                `}</style>
            </Helmet>
            <main className="page-content">
                <SlideshowSection imageList={[photo.path]}
                                  brief={title}
                                  type={SlideshowSection.type.page}
                                  title={VideoCategoryPage.phpDateToLocaleString(_created)}/>
                <div id="video">
                    <iframe
                        src={`https://player.vimeo.com/video/${video.split("/")[3]}?autoplay=1&color=ffffff&title=0&byline=0&portrait=1`}
                        frameBorder="0"
                        allow="fullscreen" allowFullScreen/>
                </div>
                <div id="content" dangerouslySetInnerHTML={{__html: content}}/>
            </main>
        </>
    }
}