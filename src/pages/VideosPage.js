import React, {Component} from "react";
import SlideshowSection from "../components/SlideshowSection";
import Breadcrumb from "../components/Breadcrumb";
import Works from "../components/Works";
import Partners from "../components/Partners";
import {Helmet} from "react-helmet";
import {SITE_NAME} from "../config";
import {Link} from "react-router-dom";

export default class VideosPage extends Component {
    render() {
        return <>
            <Helmet>
                <title>{`Videos | ${SITE_NAME}`}</title>
                <meta name="description" content="
                TODO
                "/>
                <meta name="keywords" content="Video, photo, photographer, filmmaking, Prague, "/>
                <meta name="author" content="Alikhan Mexman"/>
                <style>{`
                #show-more{
                    margin: 0 auto;
                    max-width: 13rem;
                    width: 100%;
                    display: block;
                    margin-top: -1.5rem;
                    background: #15a5eb;
                    color: white;
                    font-weight: 700;
                }
section.text-center.well.well-sm.text-lg-left {
    z-index: 99;
    background: white;
    position: relative;
}
.category.thumbnail.thumbnail-3 img {
                        width: 100% !important;
                    }

                    .col-sm-6.col-lg-3, .col-sm-6.col-lg-6 {
                        padding: 0 !important;
                    }

                    .thumbnail-3 {
                        max-width: 100%;
                        width: 99%;
                    }



                    .category.thumbnail.thumbnail-3:hover h6 {
                        color: black;
                        text-align: left;
                        padding-left: 1rem;
                        margin-bottom: 1.5rem;
                        width: 240%;
                        background: white;
                        margin-left: -1rem;
                        margin-right: -1rem;
                        transition: .3s;
                    }
                    .row-50 > * {
                        margin-bottom: 0.1rem;
                    }

                    .thumbnail-3 {
                        max-width: 100%;
                        width: 99%;
                        height: 25rem;
                        display: flex;
                        background-size: cover;
                        background-position: center;
                    }
                `}</style>
            </Helmet>
            <main className="page-content">
                <SlideshowSection type={SlideshowSection.type.page} imageList={["/images/Dd.png"]}/>
                <Breadcrumb title="Alikhan Mexman" brief="Videos" linkList={[]}/>
                <Works title="Choose Category">
                    <Works.Category size={6}
                                    title="Commercials" url="commercial"
                                    image="images/Commercials.png"/>
                    <Works.Category title="Music" url="music"
                                    image="images/Music.png"/>
                    <Works.Category title="Films" url="film"
                                    image="images/Films.png"/>
                    <Works.Category title="Events" url="event"
                                    image="images/Events.png"/>
                    <Works.Category title="Weddings" url="wedding"
                                    image="images/Weddings.png"/>
                    <Works.Category title="All" url="all"
                                    size={6}
                                    image="https://images.unsplash.com/photo-1528109966604-5a6a4a964e8d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"/>

                </Works>
                <Partners/>
            </main>
        </>
    }
}