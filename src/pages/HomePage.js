import React, {Component} from "react";
//----------------------------------------------------------------------------------------------------------------------
import Features from "../components/Features";
import ImageSection from "../components/ImageSection";
import AboutBlock from "../components/AboutBlock";
import Numbers from "../components/Numbers";
import CallToAction from "../components/CallToAction";
import Testimonials from "../components/Testimonials";
import ArticleItem from "../components/ArticleItem";
import SlideshowSection from "../components/SlideshowSection";
import Partners from "../components/Partners";
import {API, DBG_MSG, SITE_NAME} from "../config";
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";
import VideoCategoryPage from "./VideoCategoryPage";
//----------------------------------------------------------------------------------------------------------------------

export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lastWorks: []
        };

        this.initLastWorks().then(DBG_MSG);
    }

    async initLastWorks() {
        const lastWorks = [];
        const lastWorksAPI = await API.get.lastNWorks(3);

        for (let i = 0; i < lastWorksAPI.length; i++) {
            const item = lastWorksAPI[i];
            item.photo.path = await API.get.vimeoImgUrl(item.video, "large");
            lastWorks.push(item);
        }

        this.setState({lastWorks});
    }

    render() {
        const {lastWorks} = this.state;
        return <>
            <Helmet>
                <title>{`${SITE_NAME}`}</title>
                <meta name="description" content="
                TODO
                "/>
                <meta name="keywords" content="Video, photo, photographer, filmmaking, Prague, "/>
                <meta name="author" content="Alikhan Mexman"/>
                <style>{`
                .jumbotron.text-center * {
                    color: white;
                }
                .jumbotron.text-center .button {
                    background-color: #6dc3f2d1;
                }
                `}</style>
            </Helmet>
            <main className="page-content">
                <SlideshowSection title="Alikhan Mexman" brief="Filmmaker & Photographer"
                                  imageList={["/images/hello-home.png"]}>
                </SlideshowSection>
                <section className="well well-sm well-inset-2 text-center text-lg-left">
                    <div className="container">
                        <h1 className="font-weight-bold text-center">Last Works</h1>
                        <div className="row row-30">
                            {
                                lastWorks.map(({photo, url, title, category}) => <div className="col-lg-4">
                                    <Link to={`/video/${VideoCategoryPage.categoryNameToUrl(category)}/${url}`}>
                                        <ArticleItem title={title} image={photo.path}/>
                                    </Link>
                                </div>)
                            }
                        </div>
                    </div>
                </section>
                <Features title="What can you expect" features={[
                    Features.featureObject("thumbs-up", "Content Quality", <p>100% of collaborators and clients who we
                        worked with were completely satisfied with the outcome product.</p>),
                    Features.featureObject("gratipay", "Passion", <p>'Made with love' - that's where this expression is
                        the most relevant. </p>),
                    Features.featureObject("clock-o", "Speed", <p>No matter what are the scales of the project, the
                        product is to be delivered as fast as possible and almost half of the projects were made in
                        extremely short time frames. </p>),
                    Features.featureObject("code-fork", "Versatility", <p>Flexibility in terms of time, location,
                        delivery dates and budget. Your vision is always welcomed and it is the starting point. No
                        matter how big it is, we will find a way to adapt and polish it, adding that 'cherry' or 'spice'
                        (or both of them). However, you are also always able to leave the whole 'cooking' to me.</p>),
                ]}/>
                <AboutBlock title="Experience" progress={[
                    AboutBlock.progressObject("Video-editing software", 99),
                    AboutBlock.progressObject("Camera", 85),
                    AboutBlock.progressObject("VFX (visual effects)", 50),
                    AboutBlock.progressObject("Communicating", 96),
                ]}>
                    Never been to any of filming schools. 100% self-educated. My vision is to create, implement and
                    track the most advanced, beautiful and creative techniques and ways of visual processes.
                </AboutBlock>
                <Numbers list={[
                    Numbers.numberObject(21, "commercials shot"),
                    Numbers.numberObject(3280, "gigabytes used"),
                    Numbers.numberObject(200, "bananas eaten"),
                    Numbers.numberObject(11000, "photos made"),
                ]}/>
                <ImageSection title="About" image="/images/hello-landing.png">
                    Hi there ! I am Alikhan. <br/>
                    Friends call me Ali, Alex. My clients and collaborators are my friends too, so, feel free to do so
                    as well! <br/>
                    <br/>
                    I am a <Link className="text-primary" to="/video">filmmaker</Link>, <Link className="text-primary"
                                                                                              to="/photo">photographer</Link> and
                    artist currently based right in the heart of Europe, in Prague, Czech Republic 🇨🇿. I've been doing
                    this amazing and infinite craft almost for 5 years, from the age of 15, to be precise.
                </ImageSection>
                {/*<Testimonials/>*/}
                <CallToAction/>
                <Partners/>
            </main>
        </>
    }
}