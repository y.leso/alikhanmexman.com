import React, {Component} from "react";
import {Helmet} from "react-helmet";
// import { Card, Icon, Image } from 'semantic-ui-react';
import SlideshowSection from "../components/SlideshowSection";
import Loading from "../components/Loading";
import {API, DBG_MSG, mountJS, SITE_NAME} from "../config";
import {Link} from "react-router-dom";

export default class VideoCategoryPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: undefined
        };
        this.loadVideos();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        mountJS()
    }

    async loadVideos() {
        const items = [];
        const itemsAPI = await API.get.video(this.categoryURLToName());

        for (let i = 0; i < itemsAPI.length; i++) {
            const item = itemsAPI[i];
            item.photo = {path: await API.get.vimeoImgUrl(item.video)};
            items.push(item);
        }

        this.setState({items});
    }

    categoryURLToName() {
        return VideoCategoryPage
            .categoryURLToName(this.props.match.params.category);
    }

    static categoryURLToName(url) {
        switch (url) {
            case "commercial":
                return "Commercials";
            case "music":
                return "Music";
            case "event":
                return "Events";
            case "wedding":
                return "Weddings";
            case "film":
                return "Films";
            case "all":
                return undefined; // avoid filtering by category in request
            default:
                return false;
        }
    }

    static categoryNameToUrl(name) {
        switch (name) {
            case "Commercials":
                return "commercial";
            case "Music":
                return "music";
            case "Events":
                return "event";
            case "Wedding":
                return "wedding";
            case "Films":
                return "film";
            default:
                return false;
        }
    }

    static phpDateToLocaleString(date) {
        return new Date(date * 1000).toLocaleDateString(
            "en-US", {day: 'numeric', month: 'long', year: 'numeric'})
    }

    render() {
        console.log(this.state);
        const {items} = this.state;
        const {category} = this.props.match.params;
        if (items === undefined) return <Loading/>;

        return <>
            <Helmet>
                <title>{`${this.categoryURLToName()} | Videos | ${SITE_NAME}`}</title>
                <meta name="description" content="
                TODO
                "/>
                <meta name="keywords" content="Video, photo, photographer, filmmaking, Prague, "/>
                <meta name="author" content="Alikhan Mexman"/>
                <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"/>
                <style>
                    {`
                    .ui.cards.centered {
                        margin-top: unset;
                        margin-bottom: unset;
                    }
                    .ui.card .meta, .ui.cards>.card .meta {
                        font-size: 0.8rem;
                    }

                    .ui.card>.content>.header:not(.ui), .ui.cards>.card>.content>.header:not(.ui) {
                        font-size: 1.9rem;
                        margin: 0;
                    }

                    .ui.card>.content, .ui.cards>.card>.content {
                        padding: .5rem;
                    }
                    `}
                </style>
            </Helmet>
            <main className="page-content">
                <SlideshowSection title="Alikhan Mexman" brief={this.categoryURLToName() || "All Visuals"} type={SlideshowSection.type.page}/>
                {
                    items.map(({title, photo, url, _created}) => <Link to={`/video/${category}/${url}`}>
                        <div className="video-item" style={{backgroundImage: `url(${photo.path})`}}>
                            <h2>{title}</h2>
                            <span>{VideoCategoryPage.phpDateToLocaleString(_created)}</span>
                        </div>
                    </Link>)
                }
            </main>
        </>
    }
}