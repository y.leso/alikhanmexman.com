import React, {Component} from "react";
import SlideshowSection from "../components/SlideshowSection";
import Breadcrumb from "../components/Breadcrumb";
import TextBlocks from "../components/TextBlocks";
import Numbers from "../components/Numbers";
import TextSection from "../components/TextSection";
import Works from "../components/Works";
import {Helmet} from "react-helmet";
import {SITE_NAME} from "../config";
import {Link} from "react-router-dom";
import ImageSection from "../components/ImageSection";

export default class AboutPage extends Component {
    render() {
        let daysWorked = Math.ceil(
            Math.abs(
                (new Date() - new Date(2019, 2, 2))
            ) / (1000 * 60 * 60 * 24)
        );

        const years = Math.floor(daysWorked/365);
        daysWorked -= years*365;

        const month = Math.ceil(daysWorked/30);
        daysWorked -= (month-1)*30;
        return <>
            <Helmet>
                <title>{`About me | ${SITE_NAME}`}</title>
                <meta name="description" content="
                TODO
                "/>
                <meta name="keywords" content="Video, photo, photographer, filmmaking, Prague, "/>
                <meta name="author" content="Alikhan Mexman"/>
                <style>{`
                 .category.thumbnail.thumbnail-3 img {
                        width: 100% !important;
                    }

                    .col-sm-6.col-lg-3, .col-sm-6.col-lg-6 {
                        padding: 0 !important;
                    }

                    .thumbnail-3 {
                        max-width: 100%;
                        width: 99%;
                    }



                    .category.thumbnail.thumbnail-3:hover h6 {
                        color: black;
                        text-align: left;
                        padding-left: 1rem;
                        margin-bottom: 1.5rem;
                        width: 240%;
                        background: white;
                        margin-left: -1rem;
                        margin-right: -1rem;
                        transition: .3s;
                    }
                    .row-50 > * {
                        margin-bottom: 0.1rem;
                    }

                    .thumbnail-3 {
                        max-width: 100%;
                        width: 99%;
                        height: 25rem;
                        display: flex;
                        background-size: cover;
                        background-position: center;
                    }

`}</style>
            </Helmet>
            <main className="page-content">
                <SlideshowSection
                    title="Alikhan Mexman"
                    brief="About me"
                    type={SlideshowSection.type.page}
                    imageList={["/images/Dd.png"]}
                />
                <Breadcrumb title="About" linkList={[]}/>
                <ImageSection title="About" image="/images/hello-landing.png">
                    Hi there ! I am Alikhan. <br/>
                    Friends call me Ali, Alex. My clients and collaborators are my friends too, so, feel free to do so as well! <br/>
                    <br/>
                    I am a <Link className="text-primary" to="/video">filmmaker</Link>, <Link className="text-primary" to="/photo">photographer</Link> and artist currently based right in the heart of Europe, in Prague, Czech Republic 🇨🇿. I've been doing this amazing and infinite craft almost for 5 years, from the age of 15, to be precise.
                </ImageSection>
                <TextBlocks>
                    <TextBlocks.Container title="Borders never matter">
                        People we worked with are from Belgium, Kazakhstan, Czech Republic, Russia, Ukraine, Italy and
                        the list is only getting bigger!
                    </TextBlocks.Container>
                    <TextBlocks.Container title="Languages">
                        I speak English and Russian freely, so you will never have any troubles with language barrier.
                        We could even try to speak French, Uighur and Kazakh if you wish :)
                    </TextBlocks.Container>
                    <TextBlocks.Container title="Notes are not a problem">
                        Music is also my big passion. When it comes to sound design, music choosing, atmosphere setting,
                        you can freely rely all the process on me and be confident about the results.
                    </TextBlocks.Container>
                </TextBlocks>

                <TextSection titleTop="For" titleBottom="of works" brief={
                    `${years}y. ${month !== 0 ? month + "m." : ""} ${daysWorked !== 0 ? daysWorked + "d." : ""}`
                } buttonList={[
                    TextSection.buttonObject('/video', 'Videos'),
                    TextSection.buttonObject('/photo', 'Photos'),
                ]}/>
                <Numbers list={[
                    Numbers.numberObject(21, "commercials shot"),
                    Numbers.numberObject(3280, "gigabytes used"),
                    Numbers.numberObject(200, "bananas eaten"),
                    Numbers.numberObject(11000, "photos made"),
                ]}/>
                <Works title="Portfolio">
                    <Works.Category size={6}
                                    title="Commercials" url="commercial"
                                    image="images/Commercials.png"/>
                    <Works.Category title="Music" url="music"
                                    image="images/Music.png"/>
                    <Works.Category title="Films" url="film"
                                    image="images/Films.png"/>
                    <Works.Category title="Events" url="event"
                                    image="images/Events.png"/>
                    <Works.Category title="Weddings" url="wedding"
                                    image="images/Weddings.png"/>
                    <Works.Category title="All" url="all"
                                    size={6}
                                    image="https://images.unsplash.com/photo-1528109966604-5a6a4a964e8d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"/>

                </Works>
            </main>
        </>
    }
}