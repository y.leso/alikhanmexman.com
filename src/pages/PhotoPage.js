import React, {Component, useCallback} from "react";
import {API, DBG_MSG, mountJS, SITE_NAME} from "../config";
import Gallery from "react-photo-gallery";
import Carousel, {Modal, ModalGateway} from "react-images";
import Loading from "../components/Loading";
import {Helmet} from "react-helmet";
import SlideshowSection from "../components/SlideshowSection";

export default class PhotoPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewerIsOpen: false,
            image_index: 0,
            photos: undefined
        };
        //------------------------------------------------
        this.loadPhotos().then(
            DBG_MSG("Photos was loaded 👍")
        );
        //------------------------------------------------
        this.openLightbox = this.openLightbox.bind(this);
        this.closeLightbox = this.closeLightbox.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        mountJS();
    }

//------------------------------------------------------------------------------
    async loadPhotos() {
        const photos = [];
        await API.get.vkImages(178625961, '271219476')
            .then(res => res.forEach(
                ({sizes}) => {
                    const photo = sizes[sizes.length - 1];
                    photos.push({...photo, src: photo.url})
                }
            ));

        this.setState({photos})
    }

//------------------------------------------------------------------------------

    openLightbox(event, {photo, index}) {
        this.setState({
            viewerIsOpen: true,
            image_index: index
        })
    }

    closeLightbox() {
        this.setState({
            viewerIsOpen: false,
            image_index: 0
        })
    };

//------------------------------------------------------------------------------
    render() {
        const {photos, viewerIsOpen, image_index} = this.state;
        //------------------------------------------------
        if (photos === undefined) return <Loading/>;
        //------------------------------------------------

        return <>
            <Helmet>
                <title>{`Photo | ${SITE_NAME}`}</title>
                <meta name="description" content="
                TODO
                "/>
                <meta name="keywords" content="Video, photo, photographer, filmmaking, Prague, "/>
                <meta name="author" content="Alikhan Mexman"/>
                <style>{`
            .swiper-slide.swiper-slide-active:after {
                    content: "";
                    display: block;
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    left: 0;
                    right: 0;
                    background: #ffffff8c;
                }`
                }</style>
            </Helmet>
            <main className="page-content">
                <SlideshowSection type={SlideshowSection.type.page}
                                  title="Photos"
                                  imageList={photos.map(p => p.src)}
                                  brief="Last Works"/>
                <Gallery photos={photos} onClick={this.openLightbox}/>
                {viewerIsOpen && (
                    <ModalGateway>
                        <Modal onClose={this.closeLightbox}>
                            <Carousel
                                currentIndex={image_index}
                                views={photos.map(
                                    x => ({
                                        ...x, srcset: undefined, caption: x.title
                                    })
                                )}
                            />
                        </Modal>
                    </ModalGateway>
                )}
            </main>
        </>;
    }

}