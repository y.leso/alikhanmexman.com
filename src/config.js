import $ from "jquery";

const CORS = true;
const DEBUG = true;
//----------------------------------------------------------------------------------------------------------------------
export const SITE_NAME = "Alikhan Mexman";
//----------------------------------------------------------------------------------------------------------------------
export const ERROR_CATCH = console.error;
export const DBG_MSG = (msg) => DEBUG ? console.debug(msg + `\n${new Date()}`) : null;
export const GET_OPTIONS = {
    method: 'GET',
    redirect: 'follow'
};
//----------------------------------------------------------------------------------------------------------------------
export const API = {
    cms: "https://the-cesko.cz/CMS/api",
    token: {
        // cmsTheCesko VK app token
        // vk: "ec95e800ec95e800ec95e80088ec8bbcb8eec95ec95e800b2f30e55bca34278e75df21c",
        vk: "deb38bff893680097ff0e3a958c5eebb2c2c86ce32688849cbe099cc2c2e976ffddcde1af6981bb688f54",
        // a.mexman API key from cms
        cms: "account-aab59ec6d07c352d7442f5869df543"
    },
    request: (query, corse = true) => CORS && corse ? `https://cors-anywhere.herokuapp.com/${query}` : query,
    send: {
      form: (data)=>fetch(`${API.cms}/forms/submit/alikhanmexman?token=${API.token.cms}`, {
          method: 'post',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
              form: data
          })
      })
          .then(entry => entry.json())
          .then(entry => console.log(entry))
          .catch(ERROR_CATCH)
    },
    get: {
        //--------------------------------------------------------------------------------------------------------------
        vkImages: (owner_id, album_id = "wall", token = API.token.vk) => fetch(API.request(
            `https://api.vk.com/method/photos.get\
?owner_id=${owner_id}\
&album_id=${album_id}\
&rev=1\
&access_token=${token}\
&v=5.92`
        ), GET_OPTIONS).then(async response => JSON.parse(await response.text()))
            .then(async response => response.response.items)
            .catch(ERROR_CATCH),
        //--------------------------------------------------------------------------------------------------------------
        video: (category, url = undefined) => fetch(API.request(
            `${API.cms}/collections/get/ali_video?token=${API.token.cms}`, false), {
            method: "post",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                filter: {category, url}
            })
        })
            .then(res => res.json())
            .then(res => res.entries)
            .catch(ERROR_CATCH),
        lastNWorks: (limit = 3) => fetch(API.request(
            `${API.cms}/collections/get/ali_video?token=${API.token.cms}`, false), {
            method: "post",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                limit: 3
            })
        })
            .then(res => res.json())
            .then(res => res.entries)
            .catch(ERROR_CATCH),
        //--------------------------------------------------------------------------------------------------------------
        vimeoImgUrl: async (url, size="large") => {
            if (url.match("vimeo.com")) {
                return (await fetch(API.request(`//vimeo.com/api/v2/video/${
                    url.split("/").splice(-1)[0]
                    }.json?callback=showThumb`))
                    .then(res => res.text())
                    .then(res => res.slice(14, -1)) // remove redundant symbols
                    .then(res => JSON.parse(res))
                    .then(res => res[0][`thumbnail_${size}`]));
            }
            return url;
        }
    }
};

//----------------------------------------------------------------------------------------------------------------------
export function mountJS(libs = []) {
    libs = Array.isArray(libs) ? libs : [libs];
    document.getElementById("mountScr").innerHTML = "";
    const scripts_to_load = [
        "/js/core.min.js",
        "/js/script.js",
        ...libs
    ];
    scripts_to_load.forEach(
        (script_name) => $('#mountScr').append(
            `<script type="text/javascript" src="${script_name}"></script>`
        )
    );
}