import React, {Component, Fragment} from 'react';
import {Route, Link, BrowserRouter as Router, Switch, withRouter} from 'react-router-dom'
//----------------------------------------------------------------------------------------------------------------------
import Header from "./components/Header";
import Footer from "./components/Footer";
//----------------------------------------------------------------------------------------------------------------------
import HomePage from "./pages/HomePage";
import AboutPage from "./pages/AboutPage";
import VideosPage from "./pages/VideosPage";
import ContactPage from "./pages/ContactPage";
import PhotoPage from "./pages/PhotoPage";
import VideoPage from "./pages/VideoPage";
import {mountJS} from "./config";
import VideoCategoryPage from "./pages/VideoCategoryPage";

export default class App extends Component {
    componentDidMount() {mountJS()}

    render() {
        return <Router>
            <Header/>
            <Switch>
                <Route exact path="/" component={HomePage}/>
                <Route exact path="/about" component={AboutPage}/>
                <Route exact path="/video" component={VideosPage}/>
                <Route exact path="/video/:category" component={VideoCategoryPage}/>
                <Route path="/video/:category/:url" component={VideoPage}/>
                <Route exact path="/photo" component={PhotoPage}/>
                <Route exact path="/contact" component={ContactPage}/>
            </Switch>
            <Footer/>
        </Router>
    }
}
