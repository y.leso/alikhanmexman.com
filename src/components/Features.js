import React, {Component} from 'react';

export default class Features extends Component {
    static featureObject(icon, title, description){
        return {icon, title, description}
    }
    render() {
        const {title, children, features} = this.props;
        return <section className="well well-sm bg-lighter relative text-center">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-6">
                        <h1 className="font-weight-bold">{title}</h1>
                        <p className="lead">{children}</p>
                    </div>
                </div>
                <div className="row margin-1 text-lg-left">
                    {features.map(
                        ({icon, title, description}) => <div className="col-md-6 col-lg-3">
                            <span className={`icon icon-lg icon-primary fa-${icon}`}/>
                            <h5>{title}</h5>
                            {description}
                        </div>
                    )}
                </div>
            </div>
        </section>
    }
}

Features.defaultProps = {
    title: "Titile of Feauture",
    children: "",
    features: [
        Features.featureObject("heartbeat", "Feature one", <p>Lorem Ipsut Daniial Ahahaha</p>),
        Features.featureObject("heartbeat", "Feature one", <p>Lorem Ipsut Daniial Ahahaha</p>),
        Features.featureObject("heartbeat", "Feature one", <p>Lorem Ipsut Daniial Ahahaha</p>),
        Features.featureObject("heartbeat", "Feature one", <p>Lorem Ipsut Daniial Ahahaha</p>),
    ]
};