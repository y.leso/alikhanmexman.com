import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class Footer extends Component{
    render() {
        return <footer className="page-footer footer-centered text-center">
            <section className="footer-content">
                <div className="container">
                    <div className="navbar-brand"><Link to="/"><img src="/images/logo-default.png" alt=""/></Link>
                    </div>
                    <p className="big">I believe in infinity of mental and physical powers. It's not about specific stuff, it's about passion.</p>
                    <ul className="list-inline">
                        <li className="list-inline-item"><a className="fa-twitter" href="https://twitter.com/ALIKHANTHEONE"/></li>
                        <li className="list-inline-item"><a className="fa-instagram" href="https://www.instagram.com/alikhan.mexman/"/></li>
                        <li className="list-inline-item"><a className="fa-facebook" href="https://www.facebook.com/profile.php?id=100007211923481"/></li>
                    </ul>
                </div>
            </section>
            <section className="copyright">
                <div className="container">
                    <p>&#169; <span className="copyright-year"/> All Rights Reserved
                        <br/>
                        Provozovatel: Mark Limited Group, s.r.o.
                    </p>
                </div>
            </section>
        </footer>
    }
}