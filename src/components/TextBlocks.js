import React, {Component} from "react";

export default class TextBlocks extends Component {
    static Container({title, children}) {
        return <div className="col-lg-4">
            <h4 className="font-weight-bold text-uppercase">{title}</h4>
            <hr className="short bg-accent"/>
            <p>{children}</p>
        </div>
    }

    render() {
        const {children} = this.props;
        return <section className="text-center well well-sm text-lg-left">
            <div className="container">
                <div className="row row-50">
                    {children}
                </div>
            </div>
        </section>
    }
}