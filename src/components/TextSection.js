import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class TextSection extends Component {
    static buttonObject(url, title) {
        return {url, title}
    }

    render() {
        const {titleTop, titleBottom, brief, buttonList} = this.props;
        return <section className="well well-sm well-inset-2 bg-dark-var2 text-center">
            <div className="jumbotron">
                <h1>
                    <small>{titleTop}</small>
                    {brief}
                    <small>{titleBottom}</small>
                </h1>
                <div className="button-group-variant no-offset">
                    {buttonList.map(
                        ({
                             url,
                             title
                         }) => <Link className="button button-default round-xl button-sm" to={url}>{title}</Link>
                    )}
                </div>
            </div>
        </section>

    }
}