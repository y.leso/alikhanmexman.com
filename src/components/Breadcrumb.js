import React, {Component} from "react";
import {Link} from "react-router-dom";


export default class Breadcrumb extends Component{
    static linkObject(url, title){
        return {url, title};
    }
    render() {
        const {linkList, title} = this.props;
        return <ol className="breadcrumb section-border">
            <li><Link to="index.html">Home</Link></li>
            {linkList && linkList.map(
                ({url, title}) => <li><Link to={url}>{title}</Link></li>
            )}
            <li className="active">{title}</li>
        </ol>
    }
}