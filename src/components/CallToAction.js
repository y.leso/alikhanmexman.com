import React, {Component} from "react"
import {API} from "../config";

export default class CallToAction extends Component {

    onSend(){
        API.send.form({
            type: "Contact Form",
            mail: document.getElementById("forms-news-email").value
        })
    }
    render() {
        const {title, brief} = this.props;
        // TODO sending contact
        return <section className="well well-sm well-inset-3 bg-image bg-image-1 context-dark text-center">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-8">
                        <h1 className="font-weight-bold">{title}</h1>
                        <p className="lead">{brief}</p>
                        <form className="rd-mailform subscribe-form margin-1"
                              data-form-output="form-output-global"
                              data-form-type="forms" method="post" action="bat/rd-mailform.php">
                            <div className="form-wrap form-wrap-validation">
                                <input className="form-input" id="forms-news-email" type="mail" name="email"
                                       placeholder="Enter your email" data-constraints="@Required"/>
                            </div>
                            <div className="button-wrap text-center">
                                <button className="button button-primary button-xs round-xl"
                                        type="submit">Send
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    }
}

CallToAction.defaultProps = {
    title: "Do you have amazing ideas?",
    brief: "For business and other offers/projects/collabs or just a small talk, please, feel free to get in touch with me at contact@alikhanmexman.com "
};