import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class Works extends Component {
    static Category({title, image, brief, url, size=3}) {
        return <div className={`col-sm-6 col-lg-${size}`}>
            <Link to={`/video/${url}`}>
                <div className="category thumbnail thumbnail-3" style={{backgroundImage: `url(${image})`}}>
                    {/*<img className="img-rounded" src={image} alt={title}/>*/}
                    <div className="caption margin-1">
                        <h6>{title}</h6>
                    </div>
                </div>
            </Link>
        </div>
    }

    render() {
        const {title, brief, children} = this.props;
        return <section className="text-center well well-sm section-border">
            <div className="container">
                <h1 className="font-weight-bold">{title}</h1>
                <p className="lead">{brief}</p>
                <div className="row row-50">
                    {children}
                </div>
            </div>
        </section>

    }
}