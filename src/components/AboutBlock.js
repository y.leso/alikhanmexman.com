import React, {Component} from "react";

export default class AboutBlock extends Component{
    static progressObject(title, counter){
        return {title, counter}
    }
    render() {
        const {title, children, progress} = this.props;
        return <section className="well well-sm text-center text-lg-left">
            <div className="container">
                <div className="row justify-content-lg-between">
                    <div className="col-lg-6 col-xl-5">
                        <h1 className="font-weight-bold">{title}</h1>
                        <p className="lead">{children}</p>
                    </div>
                    <div className="col-lg-6 col-xl-6 text-left">
                        <div className="inset-2">
                            {progress.map(
                                ({title, counter}) =>  <div className="progress-linear">
                                    <div className="progress-linear-header">
                                        <p className="progress-linear-title">{title}</p>
                                    </div>
                                    <div className="progress-linear-body">
                                        <div className="progress-linear-bar"/>
                                        <span className="progress-linear-counter">{counter}</span>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    }
}

AboutBlock.defaultProps={
  title: "Title is here",
  children: <>Loren ipsut dolor</>,
  progress: [
      AboutBlock.progressObject("Chegoto tam", 15),
      AboutBlock.progressObject("Chegoto tam", 15),
      AboutBlock.progressObject("Chegoto tam", 15),
      AboutBlock.progressObject("Chegoto tam", 15),
  ]
};