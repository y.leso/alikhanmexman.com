import React, {Component} from "react";

export default class ImageSection extends Component {
    render() {
        const {title, children, image} = this.props;
        return <section className="text-center well well-sm">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-10">
                        <h1 className="font-weight-bold">{title}</h1>
                        <p className="lead big">{children}</p>
                        <img className="box-shadow margin-2 margin-negative" src={image} alt=""/>
                    </div>
                </div>
            </div>
        </section>
    }
}

ImageSection.defaultProps = {
    title: "Alikhan Mexman",
    children: <>Filmmaker and photographer from Prague in Czech Republic. I shoot <a className="text-primary" href="#">event
        movies</a> and image
        film. Started filmmaking at only 16 years old and now have great deal of famous clients.
        <br/><br/>It's not about filmmaking, it' about following my passion.</>,
    image: "images/index_img1.jpg"
};