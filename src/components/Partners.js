import React, {Component} from "react";

export default class Partners extends Component{
    render(){
        return <section className="well well-sm text-center section-border">
            <div className="container">
                <ul className="flex-list">
                    <li><img src="images/agency.png" alt=""/></li>
                    <li><img src="images/FClub.png" alt=""/></li>
                    <li><img src="images/JAC.png" alt=""/></li>
                    <li><img src="images/Mark-in.png" alt=""/></li>
                    <li><img src="images/Mercedes.png" alt=""/></li>
                    <li><img src="images/Retro.png" alt=""/></li>
                </ul>
            </div>
        </section>
    }
}