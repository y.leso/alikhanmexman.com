import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class Header extends Component {
    // TODO active (use this.props)
    static menuItem(title, url, active = false) {
        return <li><Link to={url}>{title}</Link></li>
    }

    render() {
        const {menuItems} = this.props;
        return <header className="page-header">
            <div className="rd-navbar-wrap">
                <nav className="rd-navbar top-panel-none-items" data-layout="rd-navbar-fixed"
                     data-sm-layout="rd-navbar-fixed"
                     data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed"
                     data-lg-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed"
                     data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static"
                     data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px"
                     data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
                    <div className="rd-navbar-inner">
                        <div className="rd-navbar-panel">
                            <button className="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar"><span/></button>
                            <div className="rd-navbar-brand">
                                <Link to="/"><img src="/images/logo-default.png" alt="Alikhan Mexman"/></Link>
                            </div>
                        </div>

                        <div className="rd-navbar-nav-wrap">
                            <ul className="rd-navbar-nav">
                                {menuItems}
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

        </header>
    }
}

Header.defaultProps = {
    menuItems: [
        Header.menuItem("Home", '/'),
        Header.menuItem("Photo", '/photo'),
        Header.menuItem("Video", '/video'),
        Header.menuItem("About", '/about'),
        Header.menuItem("Contact", '/contact'),
    ]
};
