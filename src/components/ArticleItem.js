import React, {Component} from "react";

export default class ArticleItem extends Component {
    render() {
        const {url, title, image} = this.props;
        return <article className="thumbnail thumbnail-4"
                        style={{backgroundImage: `url(${image})`}}>
            <div className="caption">
                <h4>{title}</h4>
            </div>
        </article>;
    }
}

ArticleItem.defaultProps = {
    image: "https://d33wubrfki0l68.cloudfront.net/ee52d366025571bed8dc560ba9e2482e4b8cc2e8/66c27/assets/images/06-min.jpg",
    title: "My amazing video #1",
    url: "/video/1"
};
