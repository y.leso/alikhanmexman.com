import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class SlideshowSection extends Component {
    static type = {page: 0, full: 1};

    constructor(props) {
        super(props);
        this.Images = this.Images.bind(this);
    }

    Images({imageList = this.props.imageList}) {
        return <div className="swiper-wrapper">
            {imageList.map(
                url => <div className="swiper-slide" data-slide-bg={url}>
                    <div className="swiper-slide-caption"/>
                </div>)}
        </div>
    }

    render() {
        const {title, brief, children, type} = this.props;
        if (type === SlideshowSection.type.full)
            return <section>
                <div className="swiper-container swiper-slider" data-autoplay="5000" data-slide-effect="fade"
                     data-loop="false">
                   <div className="video-bg">
                       <img src={this.props.imageList[0]} id="videosubstitute"
                            alt="Full screen background video"/>
                           <div id="videoDiv">
                               <video preload="preload" id="video" autoPlay="autoplay" loop="loop" muted>
                                   <source src="/images/Throwback2019.m4v" type="video/mp4"/>
                               </video>
                           </div>
                   </div>
                    {/*<iframe src="https://player.vimeo.com/video/400882505?controls=0&background=1"*/}
                    {/*frameBorder="0" allow="autoplay; fullscreen" allowFullScreen></iframe>*/}
                    <div className="jumbotron text-center">
                        <h1>
                            <small>{title}</small>
                            {brief}
                        </h1>
                        <p className="big">{children}</p>
                        <div className='button-group-variant'>
                            <Link className='button button-default round-xl button-sm' to='/photo'>Photo</Link>
                            <Link className='button button-default round-xl button-sm' to='/video'>Portfolio</Link>
                        </div>
                    </div>
                    <this.Images/>
                </div>
            </section>;
        if (type === SlideshowSection.type.page)
            return <section id="page-slider">
                <div className="swiper-container swiper-slider" data-autoplay="5000"
                     data-slide-effect="fade" data-loop="false">
                    <div className="jumbotron text-center">
                        <h1>
                            <small>{title}</small>
                            {brief}
                        </h1>
                        <p className="big"/>
                    </div>
                    <this.Images/>
                </div>
            </section>;
        return null;
    }
}
SlideshowSection.defaultProps = {
    title: "Mr. Mexman",
    brief: "Filmmaker & photographer",
    children: <></>,
    imageList: [
        "/images/Dd.png",
    ],
    type: SlideshowSection.type.full
};