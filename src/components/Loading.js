import React, {Component} from "react";

export default class Loading extends Component{
    render() {
        return <div className="preloader">
            <div className="preloader-body">
                <div className="cssload-container">
                    <div className="cssload-speeding-wheel"></div>
                </div>
                <p>Loading...</p>
            </div>
        </div>
    }
}