import React, {Component} from "react"

export default class Testimonials extends Component{
    static Content({children, name, brief, img}){
        return <blockquote className="quote-2">
            <img className="rounded-circle" src={img} alt=""/>
            <h6> <cite>{name}</cite></h6>
            <p className="small text-light-clr text-uppercase">{brief}t</p>
            <p className="heading-6 font-italic font-base text-base">
                <q>{children}</q>
            </p>
        </blockquote>
    }
    render() {
        const {title, description, children} = this.props;
        return <section className="well well-sm well-inset-2 text-center">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-6">
                        <h1 className="font-weight-bold text-center">{title}</h1>
                        <p className="lead">{description}</p>
                    </div>
                </div>
                <div className="row margin-1">
                    <div className="owl-carousel" data-autoplay="true" data-items="1" data-md-items="2"
                         data-sm-items="1"
                         data-lg-items="3" data-nav="true" data-margin="30" data-loop="true">
                        {children}
                    </div>
                </div>
            </div>
        </section>
    }
}

Testimonials.defaultProps = {
        title: "Look! Am cool",
        description: "Your girl is looking on me",
        children: [
            <Testimonials.Content name="Yurii Leso" brief="Cool man. Cool feedback..." img="/images/index_img5.jpg">
                Lorem Ipsut DOlor
                Lorem Ipsut DOlor
                Lorem Ipsut <b>DOlor</b>
                Lorem Ipsut DOlor
            </Testimonials.Content>,
            <Testimonials.Content name="Yurii Leso" brief="Cool man. Cool feedback..." img="/images/index_img5.jpg">
                Lorem Ipsut DOlor
                Lorem Ipsut DOlor
                Lorem Ipsut <b>DOlor</b>
                Lorem Ipsut DOlor
            </Testimonials.Content>,
            <Testimonials.Content name="Yurii Leso" brief="Cool man. Cool feedback..." img="/images/index_img5.jpg">
                Lorem Ipsut DOlor
                Lorem Ipsut DOlor
                Lorem Ipsut <b>DOlor</b>
                Lorem Ipsut DOlor
            </Testimonials.Content>
        ]
};