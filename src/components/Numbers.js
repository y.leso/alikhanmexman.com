import React, {Component} from "react"

export default class Numbers extends Component {
    static numberObject(number, title) {
        return {number, title}
    }

    static format(number) {
        return number.toString()
            .replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1");
    }

    render() {
        const size = Math.floor(12 / this.props.list.length);
        return <section className="bg-dark-var1 text-center">
            <div className="container counter-panel">
                <div className="row">
                    {this.props.list.map(
                        ({title, number}) => <div className={`col-6 col-md-6 col-lg-${size}`}>
                            <div className="counter">{number}</div>
                            <p className="text-opacity font-secondary text-uppercase">{title}</p>
                        </div>
                    )}
                </div>
            </div>
        </section>
    }
}

Numbers.defaultProps = {
    list: [
        Numbers.numberObject(123, "people we shooted"),
        Numbers.numberObject(123, "people we shooted"),
        Numbers.numberObject(123, "people we shooted"),
        Numbers.numberObject(123, "people we shooted"),
    ]
};